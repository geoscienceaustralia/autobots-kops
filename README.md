#Dockerfile for autobots-terraform
# AUTOBOTS-KOPS

## About
This repo is used to manage and deploy the autobots-kops Docker image. This image is intended to be used with Bitbucket Pipelines that build Kubernetes clusters using kops and kubectl.
